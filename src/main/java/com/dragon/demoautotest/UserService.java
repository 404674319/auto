package com.dragon.demoautotest;

import java.util.List;

/**
 * auth Dragon
 * 2019/7/1 1:02 PM
 */
public interface UserService {

    void saveUser(UserEntity u);
    List<UserEntity> getUser(String name);

}
