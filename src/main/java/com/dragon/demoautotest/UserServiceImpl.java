package com.dragon.demoautotest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * auth Dragon
 * 2019/7/1 1:05 PM
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserReproperty userReproperty;

    @Override
    public void saveUser(UserEntity u) {
        userReproperty.save(u);
    }

    @Override
    public List<UserEntity> getUser(String name) {
        return userReproperty.findUserEntitiesByName(name);
    }
}
