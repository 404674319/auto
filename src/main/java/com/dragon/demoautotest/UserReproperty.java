package com.dragon.demoautotest;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * auth Dragon
 * 2019/7/1 1:06 PM
 */
public interface UserReproperty extends JpaRepository<UserEntity, Integer> {

    List<UserEntity> findUserEntitiesByName(String name);

}
