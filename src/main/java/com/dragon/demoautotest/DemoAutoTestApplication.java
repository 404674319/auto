package com.dragon.demoautotest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages="com.dragon")
public class DemoAutoTestApplication {
    public static void main(String[] args) {
        SpringApplication.run(DemoAutoTestApplication.class, args);
    }
}
