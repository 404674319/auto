package com.dragon.demoautotest;

import javax.persistence.*;
import java.io.Serializable;

/**
 * auth Dragon
 * 2019/7/1 12:56 PM
 */
@Entity
@Table(name = "user_r_entity")
public class UserEntity implements Serializable {

    private static final long serialVersionUID = 2290773790168567187L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private String age;
    private String sex;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }
}
