package com.dragon.demoautotest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * auth Dragon
 * 2019/7/1 6:15 PM
 */
@Controller
@RequestMapping(path = "/user")
public class UserController {

    @Autowired
    private UserService userService;

    @ResponseBody
    @RequestMapping(method = RequestMethod.GET,path = "/getUser")
    public String getUser(String name){
        List<UserEntity> users = userService.getUser(name);
        if(users != null && users.size() > 0)
            return users.get(0).getName();
        return  null;
    }

    @ResponseBody
    @RequestMapping(method = RequestMethod.GET,path = "/createUser")
    public String createUser(String name){
        try{
            UserEntity user = new UserEntity();
            user.setName(name);
            user.setAge("18");
            user.setSex("女");
            userService.saveUser(user);
        }catch (Exception e){
            return "ERROR";
        }
        return "SUCCESS";
    }
}
